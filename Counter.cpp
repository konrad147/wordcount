#include "Counter.h"
#include <iostream>
#include <sstream>
#include <cstdlib>
#include <boost/thread.hpp>

#ifdef TBB
#include <tbb/tbb.h>

tbb::mutex _GetMtx;
tbb::mutex _ZapMtx;

class LicznikSlow{
	std::fstream& _refPlik;
	int& _refLiczbaSlow;
public:
	LicznikSlow(std::fstream& rPlik, int& rSlow): _refPlik(rPlik),
													_refLiczbaSlow(rSlow)
	{}

	void operator()(const tbb::blocked_range<size_t>& r) const
	{
		unsigned liczbaSlow = 0;
		std::string str = "";
		for (unsigned i = r.begin(); i != r.end(); ++i)
		{
			_GetMtx.lock();
			std::getline(_refPlik, str);
			_GetMtx.unlock();
			bool poprBialyZnak = true;
			for (unsigned j = 0; j < str.length(); ++j)
			{
				if(str[j]== ' ' || str[j]=='\t'){
					if(poprBialyZnak)
						continue;
					else{
						++liczbaSlow;
						poprBialyZnak = true;
					}
				}
				else if(j+1 == str.length()){
					++liczbaSlow;
				}
				else{
					poprBialyZnak = false;
					continue;
				}
			}
		}
		_ZapMtx.lock();
		_refLiczbaSlow += liczbaSlow;
		_ZapMtx.unlock();
	}
};


void Counter::count_all()
{
	count_characters();
	_plik.clear();
	_plik.seekg(0, std::ios::beg);
	tbb::parallel_for(tbb::blocked_range<size_t>(0, _liczbaLinii+1), LicznikSlow(_plik, _liczbaSlow));
}

#endif


#ifndef TBB

void Counter::liczSlowaWLinii()
{
	int liczbaSlow = 0;
	std::string str;
	
	while(true)
	{
		{
			boost::lock_guard<boost::mutex> blok(_mtxZapis);
			if(!_plik.eof()){
				std::getline(_plik, str);
				++_liczbaLinii;
				_liczbaZnakow += str.length() + 1;
			}
			else
				break;
		}
		bool prevWhiteChar = true;
			for(unsigned j=0;j<str.length();++j){
				if(str[j]==' ' || str[j]=='\t'){
					if(prevWhiteChar)
						continue;
					else{
						++liczbaSlow;
						prevWhiteChar = true;
					}
				}
				else if(j+1 == str.length()){
					++liczbaSlow;
				}
				else{
					prevWhiteChar = false;
					continue;
				}

			}

	{
		boost::lock_guard<boost::mutex> blok(_mtxSuma);
		_liczbaSlow += liczbaSlow;
	}
		liczbaSlow = 0;

	}
}

void Counter::count_all()
{
	boost::thread t1(&Counter::liczSlowaWLinii, this);
	boost::thread t2(&Counter::liczSlowaWLinii, this);
	boost::thread t3(&Counter::liczSlowaWLinii, this);
	boost::thread t4(&Counter::liczSlowaWLinii, this);

	t1.join();
	t2.join();
	t3.join();
	t4.join();

	--_liczbaLinii;
	--_liczbaZnakow;
}

#endif

Counter::Counter(int liczbaArg, char* argumenty[]): _liczbaSlow(0),
									 				_liczbaLinii(0),
									 				_liczbaZnakow(0),
									 				_DoZrobienia(WSZYSTKO)
{
	if(liczbaArg == 1){
		std::cout << "Nie podales nazwy pliku!" << std::endl;
		_DoZrobienia = BLAD;
	}
	else if(liczbaArg > 1){
		parsujKomendy(liczbaArg, argumenty);
		_nazwaPliku = std::string(argumenty[1]);

			_plik.open(_nazwaPliku.c_str());
		if(!_plik.good()){
			_DoZrobienia = BLAD; 
			return;
		}
	}
}



void Counter::run()
{
	switch(_DoZrobienia)
	{
		case WSZYSTKO:
			count_all();
			std::cout << _liczbaLinii << " " << _liczbaSlow << " " << _liczbaZnakow << " " << _nazwaPliku << std::endl;
		break;
		case LINIE:
			count_lines();
			std::cout << _liczbaLinii << " " << _nazwaPliku << std::endl;
		break;
		case SLOWA:
			count_all();
			std::cout << _liczbaSlow << " " << _nazwaPliku << std::endl;
		break;
		case ZNAKI:
			count_characters();
			std::cout << _liczbaZnakow << " " << _nazwaPliku << std::endl;
		break;
		case LINIESLOWA:
			count_all();
			std::cout << _liczbaLinii << " " << _liczbaSlow << " " << _nazwaPliku << std::endl;
		break;
		case LINIEZNAKI:
			count_characters();
			std::cout << _liczbaLinii << " " << _liczbaZnakow << " " << _nazwaPliku << std::endl;
		break;
		case SLOWAZNAKI:
			count_all();
			std::cout << _liczbaSlow << " " << _liczbaZnakow << " " << _nazwaPliku << std::endl;
		break;
		default:
			std::cout << "Blad argumentow" << std::endl;
	}
}

void Counter::parsujKomendy(int liczbaArg, char* argumenty[])
{
	if(liczbaArg == 5)
		return;
	if(liczbaArg == 2){
		_DoZrobienia = WSZYSTKO;
		return;
	}
	for(int i = 1; i < liczbaArg; ++i)
	{
		if((!strcmp(argumenty[i], "-lw")) || (!strcmp(argumenty[i], "-wl"))){
			_DoZrobienia = LINIESLOWA;
			break;
		}
		if((!strcmp(argumenty[i], "-lm")) || (!strcmp(argumenty[i], "-ml"))){
			_DoZrobienia = LINIEZNAKI;
			break;
		}
		if(!strcmp(argumenty[i], "-wm") || !strcmp(argumenty[i], "-wm")){
			_DoZrobienia = SLOWAZNAKI;
			break;
		}
		if(!strcmp(argumenty[i], "-lwm") ||
			!strcmp(argumenty[i], "-lmw") ||
			!strcmp(argumenty[i], "-wlm") ||
			!strcmp(argumenty[i], "-mlw") ||
			!strcmp(argumenty[i], "-wml") ||
			!strcmp(argumenty[i], "-mwl")){
			_DoZrobienia = WSZYSTKO;
			break;
		}
		if(!strcmp(argumenty[i],"-l")){
			if(_DoZrobienia == WSZYSTKO)
				_DoZrobienia = LINIE;
			else if(_DoZrobienia == SLOWA)
				_DoZrobienia = LINIESLOWA;
			else if(_DoZrobienia == ZNAKI)
				_DoZrobienia = LINIEZNAKI;
			else if(_DoZrobienia == BLAD)
				std::cout << "Cos jest zle" << std::endl;
		}
		if(!strcmp(argumenty[i], "-w")){
			if(_DoZrobienia == 0)
				_DoZrobienia = SLOWA;
			else if(_DoZrobienia == LINIE)
				_DoZrobienia = LINIESLOWA;
			else
				_DoZrobienia = SLOWAZNAKI;

		}
		if(!strcmp(argumenty[i], "-m")){
			if(_DoZrobienia == 0)
				_DoZrobienia = ZNAKI;
			else if(_DoZrobienia == LINIE)
				_DoZrobienia = LINIEZNAKI;
			else
				_DoZrobienia = SLOWAZNAKI;
		}
	}
}

void Counter::count_lines()
{
	std::string str;
	while(!_plik.eof()){
		std::getline(_plik, str);
		++_liczbaLinii;
	}
	--_liczbaLinii;
}

void Counter::count_characters()
{
	std::string str;
	while(!_plik.eof()){
		std::getline(_plik, str);
		_liczbaZnakow += str.length() + 1;
		++_liczbaLinii;
	}
	--_liczbaZnakow;
	--_liczbaLinii;
}