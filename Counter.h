#include <iostream>
#include <fstream>
#include <boost/thread.hpp>

class Counter
{
	std::string _nazwaPliku;
	int _liczbaSlow;
	int _liczbaLinii;
	int _liczbaZnakow;
	std::fstream _plik;
	boost::mutex _mtxZapis;
	boost::mutex _mtxSuma;
	enum operacje{
		WSZYSTKO = 0, LINIE = 1, SLOWA = 2, ZNAKI = 3,
		LINIESLOWA = 4, LINIEZNAKI = 5, SLOWAZNAKI = 6, BLAD = 7
	} _DoZrobienia;

	void parsujKomendy(int liczbaArg, char* argumenty[]);
	void liczSlowaWLinii();
	void count_lines();
	void count_characters();
public:
	Counter(std::string np);
	Counter(int liczbaArg, char* argumenty[]);
	void count_all();

	void run();
};